async function fetchGraphQL(text, variables) {
  
    // Fetch data from AniList's GraphQL API:
    const response = await fetch('https://graphql.anilist.co', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        query: text,
        variables,
      }),
    });
  
    // Get the response as JSON
    return await response.json();
  }
  
export default fetchGraphQL;